import { sortEmployeesAZ, isValidName } from './helpers';

test('Returns Anna before Mary', () => {
	const list = [{name: 'Mary'}, {name: 'Anna'}];
	const actualResult = JSON.stringify(sortEmployeesAZ(list));
	const expectedResult = "[{\"name\":\"Anna\"},{\"name\":\"Mary\"}]";
	expect(actualResult).toEqual(expectedResult);
});

test('Returns ana before Anna', () => {
	const list = [{name: 'Anna'}, {name: 'ana'}];
	const actualResult = JSON.stringify(sortEmployeesAZ(list));
	const expectedResult = "[{\"name\":\"ana\"},{\"name\":\"Anna\"}]";
	expect(actualResult).toEqual(expectedResult);
});

test('Validates name to be true', () => {
	const name = "Michael Bainsfair";
	expect(isValidName(name)).toBe(true);
});

test('Validates "SELECT *" to be false', () => {
	const name = "SELECT *";
	expect(isValidName(name)).toBe(false);
});
