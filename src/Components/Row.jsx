import { FaUserAlt, FaTrash } from 'react-icons/fa';

const Row = ({employee, removeEmployee}) => {
	const deleteEmployee = () => removeEmployee(employee.id);

	return (
		<tr>
			<td>
				<span className="Table-user-icon">
					<FaUserAlt />
				</span>
				<span>
					{employee.name}
				</span>
			</td>
			<td>{employee.email}</td>
			<td>
				<button
					className="button small"
					onClick={deleteEmployee}
					aria-label="Delete employee">
					<FaTrash />
				</button>
			</td>
		</tr>
	);
};

export default Row;
