import React, {useState} from 'react';
import './Input.css';
import { isValidName } from '../helpers';

const InputField = ({addEmployee}) => {
	const [userInput, setUserInput] = useState({name: '', email: ''});

	const handleChange = (field) => (e) => {
		setUserInput({...userInput, [field]: e.target.value});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (!isValidName(userInput.name)) return;
		addEmployee(userInput);
		setUserInput({name: '', email: ''});
	}

	return (
		<div className="Input">
			<form
				onSubmit={handleSubmit}
				aria-label="Employee information">
				<label
					className="Input-field input label"
					htmlFor="name">
					Name
					<input
						className="Input-text input text"
						id="name"
						type="text"
						value={userInput.name}
						onChange={handleChange('name')} 
						placeholder="Name..."
						autoComplete="off"
						aria-label="Employee name"
						aria-required="true" />
				</label>

				<label
					className="Input-field input label"
					htmlFor="email">
					Email
					<input
						className="Input-text input text"
						id="email"
						type="text"
						value={userInput.email}
						onChange={handleChange('email')} 
						placeholder="Email..."
						autoComplete="off"
						aria-label="Employee email" />
				</label>
				
				<input
					className="Input-button button primary"
					type="submit"
					value="Add name" />
			</form>
		</div>
	);
};

export default InputField;

// function isValidName(name) {
// 	const regex = /^[A-Za-z]{1,}[A-Za-z\.,\-' ]+$/;
// 	return regex.test(name);
// }
