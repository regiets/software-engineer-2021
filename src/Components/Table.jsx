import { useState } from 'react';
import { FaSortAlphaDown } from 'react-icons/fa';
import Row from './Row';
import './Table.css';
import { sortEmployeesAZ } from '../helpers';

const Table = ({employees, removeEmployee}) => {
	const [sortAZ, setSortAZ] = useState(false);

	const toggleSortAZ = () => {
		setSortAZ(!sortAZ);
	};

	const sortedEmployees = sortAZ ? sortEmployeesAZ(employees) : employees;

	return (
		<div className="Table">
			<table>
				<thead>
					<tr>
						<th className="Table-header first col-5">
							Name
							<button
								className={sortAZ ? "button small active" : "button small"}
								onClick={toggleSortAZ}
								aria-pressed={sortAZ ? "true" : "false"}
								aria-label="Alphabetize table">
								<FaSortAlphaDown />
							</button>
						</th>
						<th className="Table-header col-5">Email</th>
						<th className="Table-header last col-2">Actions</th>
					</tr>
				</thead>
				<tbody className="Table-body">
						{sortedEmployees.map((employee) => (
							<Row
								key={employee.id}
								employee={employee}
								removeEmployee={removeEmployee} />
						))}
				</tbody>
			</table>
		</div>
	);
};

export default Table;
