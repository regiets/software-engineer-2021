import { useState } from 'react';
import Input from './Components/Input';
import Table from './Components/Table';
import './App.css';

const initialEmployees = [
	{id: 123, name: "Jonathan", email: 'jonathan@apolitical.co'},
	{id: 234, name: "Mary", email: 'mary@apolitical.co'},
	{id: 345, name: "Thomas", email: 'thomas.fitzgerald@apolitical.co'}
];

const App = () => {
	const [employees, setEmployees] = useState(initialEmployees);

	const addEmployee = (employee) => {
		const updatedEmployees = [...employees, {id: Date.now(), ...employee}];
		setEmployees(updatedEmployees);
	};

	const removeEmployee = (id) => {
		const filteredEmployees = employees.filter((employee) => {
			return id !== employee.id;
		});
		setEmployees(filteredEmployees);
	}

  return (
    <div className="App">
			<header>
				<h1 className="App-title">List of employees</h1>
			</header>

			<main>
				<Input addEmployee={addEmployee} />
				<Table employees={employees} removeEmployee={removeEmployee} />	
			</main>
    </div>
  );
}

export default App;
