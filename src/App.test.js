import { render, screen } from '@testing-library/react';
import App from './App';

test('Renders title', () => {
  render(<App />);
  const linkElement = screen.getByText('List of employees');
  expect(linkElement).toBeInTheDocument();
});

test('Renders submit button', () => {
  render(<App />);
  const linkElement = screen.getByText('Add name');
  expect(linkElement).toBeInTheDocument();
});
