export function sortEmployeesAZ(employees) {
	const copy = [...employees];
	copy.sort((a, b) => {
		const lowerA = a.name.toLowerCase();
		const lowerB = b.name.toLowerCase();
		if (lowerA < lowerB) return -1;
		if (lowerA > lowerB) return 1;
		return 0;
	});
	return copy;
}

export function isValidName(name) {
	const regex = /^[A-Za-z]{1,}[A-Za-z.,\-' ]+$/;
	return regex.test(name);
}
