# Todos

1. ✓ Add a Title
2. ✓ Create a table for already-entered names
3. ✓ Create an input field and button to add new employees
4. ✓ Show newly added employees in the table
5. ✓ Enable alphabetic sorting of employee names
6. ✓ Style the page to make it visually appealing
7. ✓ Style the page to make it responsive
8. ✓ Limit input data to names only

## Optional todos

1. ✓ Support input of email addresses
2. ✓ Display email addresses next to names
3. ✓ Remove employee details from list
4. Edit employee details in list
5. ✓ Add icon next to each employee name
6. ✓ Add unit test/tests
